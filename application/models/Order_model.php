<?php

class Order_model extends CI_Model
{
	public function getOrderList(int $pageNumber)
	{
		$this->db->limit(20, (($pageNumber - 1) * 20));
		return $this->db->select('soi.ord_id, soi.ord_date, soi.ord_price')
						->from('sho_order_iso soi')
						->join('sho_reference_iso sri', 'soi.ord_id = sri.rei_ord_id')
						->group_by('soi.ord_id')
						->having('COUNT(*) >', '2')
						->get()
						->result();
	}

	public function getOrderNumber()
	{
		return $this->db->select('soi.ord_id, soi.ord_date, soi.ord_price')
						->from('sho_order_iso soi')
						->join('sho_reference_iso sri', 'soi.ord_id = sri.rei_ord_id')
						->group_by('soi.ord_id')
						->having('COUNT(*) >', '2')
						->count_all_results();
	}
}
