<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isset($_GET['page']) && $_GET['page'] < 1){
	header('Location: http://localhost:8000/');
}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}

		table {
			width: 100%;
		}

		.pager {
			position: relative;
			text-align: center;
			list-style: none;
		}

		.pager li {
			display: inline-block;
			list-style: none;
			margin-right: 5px;
		}
	</style>
</head>
<body>

<div id="container">
	<h1>Ma liste de commandes</h1>

	<div id="body">
		<table>
			<thead>
			<tr>
				<td>ID commande (ord_id)</td>
				<td>Date commande (ord_date)</td>
				<td>Montant de la commande (ord_price)</td>
			</tr>
			</thead>
			<tbody>
			<!-- content loaded with AJAX -->
			</tbody>
		</table>
	</div>

	<div class="item-list">
		<ul class="pager">
			<!-- list created with AJAX -->
		</ul>
	</div>
</div>

<script>
fetch('http://localhost:8000/index.php/Welcome/getResult/<?= isset($_GET['page']) ?$_GET['page']: 1; ?>')
		.then(response => {
			response.json()
					.then(data => {
						if (data.length === 0) location.href = 'http://localhost:8000/'
						const tbody = document.querySelector('#body table tbody')

						data.forEach(order => {
							const row = document.createElement('tr')

							const columnId = document.createElement('td')
							columnId.innerText = order.ord_id
							row.appendChild(columnId)

							const columnDate = document.createElement('td')
							columnDate.innerText = order.ord_date
							row.appendChild(columnDate)

							const columnPrice = document.createElement('td')
							columnPrice.innerText = order.ord_price
							row.appendChild(columnPrice)

							tbody.appendChild(row)
						})
					})
					.catch(error => {
						console.log(error)
					})
		})
		.catch(error => {
			console.log(error)
		})
fetch('http://localhost:8000/index.php/Welcome/getOrderNumber')
		.then(response => {
			response.json()
					.then(data => {
						const maxPage = Math.ceil(data / 20)
						localStorage.setItem('pageNumber', maxPage)
						const result = /^\?page=(?<pageNumber>[\d]+)$/.exec(location.search)
						let { pageNumber } = result ? result.groups : { pageNumber: 1}
						pageNumber = pageNumber ? parseInt(pageNumber) : pageNumber

						const listContainer = document.querySelector('.item-list .pager')

						if (pageNumber !== 1) {
							const liFirstItem = document.createElement('li')
							liFirstItem.classList.add('pager-first')
							liFirstItem.classList.add('first')
							const liFirstLink = document.createElement('a')
							liFirstLink.href = `http://localhost:8000/?page=1`
							liFirstLink.title = `Aller à la première page`
							liFirstLink.innerText = 'Première'
							liFirstItem.appendChild(liFirstLink)
							listContainer.appendChild(liFirstItem)

							const liPrevious = document.createElement('li')
							liPrevious.classList.add('pager-previous')
							const linkPrevious = document.createElement('a')
							linkPrevious.href = `http://localhost:8000/?page=${pageNumber - 1}`
							linkPrevious.innerText = '<'
							liPrevious.appendChild(linkPrevious)
							listContainer.appendChild(liPrevious)

							const liEllipsisPrevious = document.createElement('li')
							liEllipsisPrevious.classList.add('pager-ellipsis')
							liEllipsisPrevious.innerText = '...'
							listContainer.appendChild(liEllipsisPrevious)
						}

						let i = pageNumber ? pageNumber : 1
						let max = pageNumber ? pageNumber + 4 : 5
						max = max > maxPage ? maxPage : max
						for (i; i <= max; i++) {
							const liItem = document.createElement('li')

							if (i === (pageNumber ? pageNumber : 1)) {
								liItem.classList.add('pager-current')
								liItem.classList.add('first')
								liItem.innerText = `${i}`
							} else {
								liItem.classList.add('pager-item')
								const link = document.createElement('a')
								link.href = `http://localhost:8000/?page=${i}`
								link.title = `Aller à la page ${i}`
								link.innerText = `${i}`
								liItem.appendChild(link)
							}

							listContainer.appendChild(liItem)
						}

						if (pageNumber !== maxPage) {
							const liEllipsisNext = document.createElement('li')
							liEllipsisNext.classList.add('pager-ellipsis')
							liEllipsisNext.innerText = '...'
							listContainer.appendChild(liEllipsisNext)

							const liNext = document.createElement('li')
							liNext.classList.add('pager-next')
							const linkNext = document.createElement('a')
							linkNext.href = `http://localhost:8000/?page=${pageNumber + 1}`
							linkNext.innerText = '>'
							liNext.appendChild(linkNext)
							listContainer.appendChild(liNext)

							const liLastItem = document.createElement('li')
							liLastItem.classList.add('pager-last')
							liLastItem.classList.add('last')
							const liLastLink = document.createElement('a')
							liLastLink.href = `http://localhost:8000/?page=${maxPage}`
							liLastLink.title = `Aller à la dernière page`
							liLastLink.innerText = 'Dernier'
							liLastItem.appendChild(liLastLink)
							listContainer.appendChild(liLastItem)
						}
					})
					.catch(error => {
						console.log(error)
					})
		})
		.catch(error => {
			console.log(error)
		})
</script>

</body>
</html>
