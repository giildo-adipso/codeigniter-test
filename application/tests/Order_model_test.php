<?php


class Order_model_test extends TestCase
{
	/**
	 * @var Order_model
	 */
	private Order_model $model;

	protected function setUp()
	{
		$this->resetInstance();
		$this->CI->load->model('Order_model');
		$this->model = $this->CI->Order_model;
	}

	public function test_get_orders()
	{
		$this->assertEquals(count($this->model->getOrderList(1)), 20);
	}

	public function test_get_orders_number()
	{
		$this->assertEquals($this->model->getOrderNumber(), 32919);
	}
}
