## HTACCESS 

```apacheconf
Options +FollowSymlinks  
# sert pour l'URL rewriting = active les liens symboliques  

RewriteEngine On  
# active la réécriture d'URL dans le fichier  

AuthName "Page protegee"  
# Permet d'afficher le message si une personne doit entrer un MdP  
AuthType Basic  
# Définie le type d'authentification  
AuthUserFile "/var/www/vhosts/vps229260.ovh.net/v2/adipso.com/www/.htpasswd"  
# Définie le fichier avec lequel l'authentification se fera  
Require valid-user  
# Indique qu'un User authentifié via le fichier ci-dessus  

AddDefaultCharset UTF-8  
# Définie l'encryptage par défaut à la norme UTF-8  
RewriteCond %{HTTP_HOST} !^(www.adipso.com|m.adipso-test.com|m.adipso.com)$  
# Si le nom de domaine est l'un des trois indiqué on entre dans la réécriture ci-dessous  
RewriteRule ^(.*) http://www.adipso.com/$1 [QSA,L,R=301]  
# On prend l'URL complète captée par la condition ci-dessus et on la renvoie vers http://www.adipso.com

Addtype font/opentype .otf  
Addtype font/truetype .ttf  
# Indique au serveur les MIMES type en fonction de l'extension  


<IfModule mod_expires.c>  
	ExpiresActive On  
	ExpiresByType font/opentype "access plus 1 month"  
	ExpiresByType font/truetype "access plus 1 month"  
	ExpiresByType image/gif "access plus 1 month"  
	ExpiresByType image/jpeg "access plus 1 month"  
	ExpiresByType image/png "access plus 1 month"  
	ExpiresByType text/css "access plus 1 week"  
	ExpiresByType text/javascript "access plus 1 week"  
	ExpiresByType application/javascript "access plus 1 week"  
	ExpiresByType application/x-javascript "access plus 1 week"  
	ExpiresByType image/x-icon "access plus 1 year"  
	ExpiresByType application/x-shockwave-flash "access plus 1 year"  
</IfModule>
# Indique la politique de cache en fonction des types de fichier

FileETag INode MTime Size
<IfModule mod_deflate.c>
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE font/opentype
AddOutputFilterByType DEFLATE font/truetype
AddOutputFilterByType DEFLATE text/javascript
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript
</IfModule>
# Définie la politique de "DEFLATE" (compression des fichiers) pour optimiser les sources

RewriteRule ^sitemap.xml$ sitemap.php [L]
# Si on charge "sitemap.xml" le serveur chargera "sitemap.php"
RewriteRule ^$ front.html [L]
# Si l'URI est vide on charge le fichier "front.html"

RewriteCond $1 !^(index\.html|front\.php|sitemap\.php|iframe\.html|scpia|baumalu|bascule\.html|commun_clients|public|_fonts|lib|_m|_m2|google_map_api\.php|download\.php|public_files|_images|admin|_template|_mobile|_js|_aws|_css|_flash|_newsletter|robots\.txt|cookiechoices\.js|pinterest-d2dc7.html)
RewriteRule ^(.*)$ front.php/$1 [L]
# Si l'URI matche avec un fichier listé dans la condition, on renvoie l'utilisateur vers "front.php/leNomDuFichier"

ErrorDocument 404 /home/www/client/exemples/404.html
# Si on tombe sur la 404 on renvoie vers un fichier spécifique pour afficher une page 404

RewriteBase /
# Ajoute un "/" à la fin des URL

# Les Flags [L] permettent de s'arrêter là si on est entré dans la condition
```
